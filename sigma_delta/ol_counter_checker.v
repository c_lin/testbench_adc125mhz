/*





*/
module ol_counter_checker
( 
   clk               , // system clock
   reset             ,
   in                ,
   err_cnt                   
);

input  wire         clk;
input  wire         reset;
input  wire [15 :0] in;
output reg  [15 :0] err_cnt;

reg         [15 :0] pre_in;

always @(posedge clk)
begin

   err_cnt = ( err_cnt < 16'hFFFF && (in - pre_in)!=1 ) ? 
             err_cnt + 1 : err_cnt; 

   /// reset counter 
   if( reset==1'b1 ) begin
      err_cnt = 0;
   end
   
   pre_in = in;
    
end

endmodule