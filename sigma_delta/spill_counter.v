/*





*/
module spill_counter
( 
   clk               , // system clock
   reset             ,
   in                ,
   count           
);

input  wire         clk;
input  wire         reset;
input  wire         in;
output reg  [15 :0] count;

reg        pre_in;

always @(posedge clk)
begin

   count = (pre_in==1'b0 && in==1'b1 && count<16'hFFFF ) ?
            count + 1 : count;

   /// reset counter 
   if( reset==1'b1 ) begin
      count = 0;
   end
   
   pre_in = in;
    
end

endmodule